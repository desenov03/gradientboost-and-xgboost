import pandas as pd
import scipy
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, classification_report
from sklearn.preprocessing import LabelEncoder
import xgboost as xgb
from scipy.sparse import hstack

data = {
    'Position': ["Java Backend Developer (Junior/Middle)", "System administrator", "IT Intern", "Delphi Developer Intern",
                 "Backend developer (Java/Go)", "Flutter developer", "Python-backend (Middle / Senior)",
                 "System administrator", "iOS developer", "DevOps intern", "Python developer", ".NET developer",
                 "Designer", "Unreal Engine Developer", "Tech Product Manager", "1С developer", "UX/UI Designer",
                 "Data Scientist", "IT speciality", "Project Manager", "System administrator", "Analytic",
                 "PHP developer", "Web developer", "PHP Backend Developer", "Designer", "C# developer",
                 "Data Engineer", "JavaScript developer", "Frontend developer", "Frontend developer", "Java developer",
                 "Senior UI/UX", "Frontend Lead", "Senior Data Engineer", "Python teacher", "Tilda developer",
                 "Flutter developer", "Flutter developer", "Python developer", "Web developer", "Intern to Data Analytics",
                 "Trainee C#/.NET developer", "1C Intern", "iOS developer intern", "System administrator",
                 "1C developer", "Graphic designer", "SQL developer"],
    'Company': ["Xbase", "Zaman 01", "Halyk Bank", "EPAM Kazakhstan", "SOMNIUM", "Digital Business Services",
                "IDSOFT", "Atlant Building KZ", "WOOPPAY", "EPAM Kazakhstan", "Kolesa Group", "EPAM Kazakhstan",
                "Aleks Asu", "Fntastic", "Relog.kz", "Profi Soft", "Smart Parking Technologies", "AVIATA.KZ",
                "Alpha Bank", "ИНЖИНИРИНГОВАЯ КОМПАНИЯ КАЗГИПРНЕФТЕТРАНС", "KMF", "Firkan", "IT Park", "EVRiKA",
                "Satti Logistics", "Kemel Bolashaq", "KMGP", "TechInvest", "Citicom Systems", "Leopart Center",
                "Bugin Holding", "InfoCart", "SMART CITIES", "Beeline", "TECHNODOM Operator", "Morrison Academy",
                "Центр развития предпринимателей", "IQSAT", "Lotomatic", "BBMINDS", "BTS Digital", "PwC", "AWWCOR",
                "3S Consulting", "Crystal Spring", "Greenhouse-Qaztomat", "REDPARK", "JS Qazaqstan", "ASPEX"],
    'Salary': [200000, 200000, None, None, 600000, 450000, 700000, 150000, None, None, 700000, None, None, None,
               750000, 300000, 600000, None, None, 750000, 300000, 120000, 400000, None, 500000, 100000, None, None,
               300000, None, 250000, 900000, 150000, 750000, None, None, None, 75000, None, None, 300000, None, None,
               50000, 50000, 50000, None, None, 250000],
    'City': ["Almaty", "Nur-Sultan", "Almaty", "Almaty", "Nur-Sultan", "Nur-Sultan", "Almaty", "Karagandy", "Karagandy",
             "Almaty", "Almaty", "Aktobe", "Aktobe", "Remote", "Almaty", "Kostanay", "Almaty", "Almaty", "Aktau", "Aktau",
             "Kyzylorda", "Kyzylorda", "Ural", "Shymkent", "Shymkent", "Shymkent", "Atyrau", "Atyrau", "Atyrau", "Almaty",
             "Almaty", "Nur-Sultan", "Almaty", "Almaty", "Almaty", "Nur-Sultan", "Almaty", "Nur-Sultan", "Almaty", "Almaty",
             "Nur-Sultan", "Almaty", "Nur-Sultan", "Nur-Sultan", "Nur-Sultan", "Almaty", "Nur-Sultan", "Nur-Sultan", "Almaty"],
    'ExperienceYear': [1, 1, 0, 0, 3, 1, 1, 1, 1, 0, 1, 3, 1, 0, 6, 6, 3, 0, 1, 6, 1, 3, 1, 1, 1, 1, 1, 1, 0,
                      6, 1, 0, 6, 6, 1, 0, 0, 3, 0, 3, 1, 0, 0, 0, 0, 0, 1, 1, 2],
    'Employment': ["Full", "Full", "Internship", "Internship", "Full", "Full", "Full", "Full", 'None', "Internship",
                   "Full", "Full", "Full", 'None', "Full", "Full", "Full", 'None', "Full", "Full", "Full", "Full", "Full",
                   "Full", "Full", "Part-time", "Full", "Part-time", "Part-time", "Full", "Full", "Full", "Full", "Full",
                   "Full", "Part-time, remote", "Full", "Project, remote", "Project, full", "Part-time, full", "Full",
                   "Internship", "Internship", "Internship", "Internship", "Part-time, full", "Part-time, remote", "Part-time, remote", "full"],
    'Key skills': ["Java, Spring, SQL", "Active directory, PC setting, Setting network connections",
                   "Desire of learning, Enthusiasm, Analytic skills", "English, SQL, Git",
                   "Python, Git, Docker", "Flutter, Dart, Figma", "Russian, Python, Django",
                   "Sociability, Responsibility, Attention to detail", 'None',
                   "Linux, Python, English", "Python, Docker, API", "C#, .NET Framework, Azure",
                   "CorelDRAW, MS PowerPoint, Sociability", 'None',
                   "Web programming, Scrum, Power BI", "1C, АРМ, Teamwork", "Figma, Adobe Photoshop, UI",
                   "Analytic skills, SQL, Database", "TCP/IP, DHCP, Setting DNS",
                   "English, MS PowerPoint, Time management", 'None', "Setting PC, Setting network connections, PC ",
                   'None', "CSS, HTML, PHP", "Git, MySQL, PHP", "OOP, PHP, MySQL",
                   "Adobe Photoshop, CorelDRAW, Instagram", 'None', 'None', "ETL, Python, SQL", "JS, HTML, CSS",
                   "Kazakh, JS, Git", "C++, Java, Android", "Figma, UI, UX", "Agile, Teamleading, React",
                   "ABAP, PostgreSQL, MySQL", "Python, creativity, learning skills", "CSS, Tilda, CorelDRAW",
                   "Android, Flutter, Dart", "CSS, JS, Bootstrap", "JS, Python, SQL",
                   "PC user, Tilda, Grammatically correct speech",
                   "English, Data Analysis, MS PowerPoint", ".NET, OOP, C#", 'None', "MVC, iOS, Swift",
                   "Setting PC, Setting network connections, PC ", "1C", "Corel Painter, teamwork, CorelDRAW"
                    ],
    'Date of publication': ["18.12.2021", "18.12.2021", "2.12.2021", "8.12.2021", "18.12.2021", "14.12.2021",
                            "18.12.2021", "10.12.2021", "25.11.2021", "17.12.2021", "15.12.2021", "20.11.2021",
                            "19.11.2021", "16.12.2021", "18.12.2021", "26.11.2021", "15.12.2021", "17.12.2021",
                            "14.12.2021", "30.11.2021", "15.12.2021", "13.12.2021", "14.12.2021", "24.11.2021",
                            "26.11.2021", "26.11.2021", "26.11.2021", "2.12.2021", "15.12.2021", "10.12.2021",
                            "29.11.2021", "16.12.2021", "8.12.2021", "8.12.2021", "8.12.2021", "16.12.2021",
                            "21.11.2021", "4.12.2021", "2.12.2021", "26.11.2021", "29.11.2021", "16.12.2021",
                            "18.12.2021", "10.12.2021", "22.11.2021", "2.12.2021", "22.11.2021", "18.12.2021",
                            "22.11.2021"],
    'Category': ["Medium Salary", "Medium Salary", "Low Salary", "Low Salary", "High Salary", "Medium Salary",
                 "High Salary", "Low Salary", "Low Salary", "Low Salary", "High Salary", "Low Salary", "Low Salary",
                 "Low Salary", "High Salary", "Medium Salary", "High Salary", "Low Salary", "Low Salary", "High Salary",
                 "Low Salary", "Low Salary", "Medium Salary", "Medium Salary", "Medium Salary", "Low Salary", "Low Salary",
                 "Low Salary", "Low Salary", "Medium Salary", "Low Salary", "Medium Salary", "High Salary",
                 "Low Salary", "Low Salary", "Low Salary", "Low Salary", "Low Salary", "High Salary", "High Salary",
                 "Low Salary", "Low Salary", "Medium Salary", "Low Salary", "Low Salary", "Low Salary", "Low Salary",
                 "Low Salary", "Low Salary"]
}

df = pd.DataFrame(data)


label_encoder = LabelEncoder()
df['Category'] = label_encoder.fit_transform(df['Category'])

tfidf_vectorizer = TfidfVectorizer(max_features=50)
X_tfidf = tfidf_vectorizer.fit_transform(df['Key skills'].fillna(''))

X_numeric = df[['ExperienceYear']]

label_encoders = {}
categorical_columns = ['City', 'Employment']
for column in categorical_columns:
    label_encoders[column] = LabelEncoder()
    X_encoded = label_encoders[column].fit_transform(df[column])

X_encoded = X_encoded.reshape(-1, 1)

X = scipy.sparse.hstack([X_tfidf, X_numeric, X_encoded], format='csr')

X_train, X_test, y_train, y_test = train_test_split(X, df['Category'], test_size=0.2, random_state=42)

model = xgb.XGBClassifier()
model.fit(X_train, y_train)

y_pred = model.predict(X_test)

accuracy = accuracy_score(y_test, y_pred)
print(f"Accuracy: {accuracy}")

print(classification_report(y_test, y_pred, zero_division=1))